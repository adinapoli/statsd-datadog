DataDog-flavored StatsD client.

Built according to specs at <http://docs.datadoghq.com/guides/dogstatsd/>.

Can be used with any statsd-compatible server if you stick to its limitations and easily extended to match exotic metric types and subprotocols.

Metrics supported:

  * Gauges
  * Counters
  * Histograms
  * Sets
  * Timers

Events... are supported too.

```haskell

import Network.StatsD

main = do
    sd <- connectStatsD "localhost" "8125"

    sendStatsDIO sd (counter_ "launches" `tagged` [ "datadog", "demo" :: Text ])

    sendStatsDIO sd (event "Datadog extensions" "DD Agent provides a statsd protocol extension for events.")
        { eSourceType = Just "haskell"
        }
```

With application monads providing stored `StatsD` things look even prettier:

```haskell

getHomeR :: Handler Html
getHomeR = do
    statsd (counter_ "site.visitors")
    defaultLayout $(widgetFile "home")
```
